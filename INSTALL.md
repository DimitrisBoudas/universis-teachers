### universis-teachers Installation

## Prerequisites

Node.js version >6.14.3 is required. Visit [Node.js](https://nodejs.org/en/)
and follow the installation instructions provided for your operating system.

## Installation

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8. To install angular-cli execute

    npm install -g @angular/cli

You may need to run it as root.


Navigate to application directory and execute:

    npm i

## Development server

- Run `ng serve` for a devevelopment server
- Navigate to `http://localhost:7002/`

The app will automatically reload if you change any of the source files.
