## This repo is under heavy development

# UniverSIS
[UniverSIS](https://universis.gr) is a coordinated effort by Greek academic institutions to build a Student Information System as an open source platform. The target is to serve our common needs to support academic and administrative processes.

## UniverSIS-teachers
Teacher application features

* Teacher dashboard for current courses/exams
* Teacher personal profile info
* Semester classes  and previous teaching assignments
* Management of current class, class profile, students, grades
* Thesis info

## Installation
  Installation instructions can be found in the [installation file](INSTALL.md)
