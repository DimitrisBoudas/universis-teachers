import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';
import {BsDropdownModule, BsModalRef, BsModalService, ModalBackdropComponent} from 'ngx-bootstrap';
import { CoursesRoutingModule } from './courses-routing.module';
import { CoursesHomeComponent } from './components/courses-home/courses-home.component';
import { CoursesRecentComponent } from './components/courses-recent/courses-recent.component';
import { CoursesDetailsComponent } from './components/courses-details/courses-details.component';
import { CoursesDetailsTabsComponent } from './components/courses-details-tabs/courses-details-tabs.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CoursesDetailsGeneralComponent} from './components/courses-details/courses-details-general.component';
import {CoursesDetailsGradingComponent} from './components/courses-details/courses-details-grading.component';
import {CoursesDetailsStudentsComponent} from './components/courses-details/courses-details-students.component';
import {CoursesHistoryComponent} from './components/courses-history/courses-history.component';
import { NgPipesModule} from 'ngx-pipes';
import { CoursesGradesModalComponent } from './components/courses-grades-modal/courses-grades-modal.component';
import { CoursesGradesStep1Component } from './components/courses-grades-modal/courses-grades-step-1.component';
import { CoursesGradesStep2Component } from './components/courses-grades-modal/courses-grades-step-2.component';
import { CoursesGradesStep3Component } from './components/courses-grades-modal/courses-grades-step-3.component';
import {SharedModule} from '../shared/shared.module';
import {CoursesSharedModule} from './courses-shared.module';
import { TabsModule } from 'ngx-bootstrap';
import {CoursesDetailsExamComponent} from './components/courses-details/course-details-exam.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CoursesRoutingModule,
    CoursesSharedModule,
    TranslateModule,
    SharedModule,
    BsDropdownModule,
    NgPipesModule,
    ReactiveFormsModule,
    TabsModule.forRoot()
  ],
  declarations: [
    CoursesHomeComponent,
    CoursesRecentComponent,
    CoursesDetailsComponent,
    CoursesDetailsGradingComponent,
    CoursesDetailsStudentsComponent,
    CoursesDetailsTabsComponent,
    CoursesDetailsGeneralComponent,
    CoursesDetailsExamComponent,
    CoursesHistoryComponent,
    CoursesGradesModalComponent,
    CoursesGradesStep1Component,
    CoursesGradesStep2Component,
    CoursesGradesStep3Component
  ],
  providers: [
    CoursesGradesModalComponent
  ],
  entryComponents: [
    CoursesGradesModalComponent
  ],
  exports: [
    CoursesGradesModalComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class CoursesModule {
  constructor(private _translateService: TranslateService) {
    //
  }
}
