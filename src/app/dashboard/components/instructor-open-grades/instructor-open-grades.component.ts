import { Component, OnInit } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {CoursesService} from '../../../courses/services/courses.service';
import {ErrorService} from '../../../error/error.service';

@Component({
  selector: 'app-instructor-open-grades',
  templateUrl: './instructor-open-grades.component.html',
  styleUrls: ['./instructor-open-grades.component.scss']
})
export class InstructorOpenGradesComponent implements OnInit {

  public openCourseExams: any = []; // Data
  public isLoading = true;          // Only if data is loaded


  constructor(private _context: AngularDataContext,
              private _coursesService: CoursesService,
              private _errorService: ErrorService) { }

  ngOnInit() {
    this._coursesService.getCourseCurrentExams().then((res) => {
      this.openCourseExams = res.value.filter ( x => x.status.alternateName !== 'closed');

      // sort classes by last period
      this.openCourseExams.forEach((x) => {
        if (x.classes && x.classes.length > 0) {
          x.classes.sort((a, b) => {
            return a.courseClass.period.id > b.courseClass.period.id ? -1 : 1;
          });
          // set courseExam url using latest courseClass
          x.url = `/courses/${x.course.id}/${x.classes[0].courseClass.year.id}/${x.classes[0].courseClass.period.id}/exams/${x.id}`;

        }
      });
      this.isLoading = false;
    }).catch(err => {
      return this._errorService.navigateToError(err);
    });

  }

}
